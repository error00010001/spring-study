package com.train.services;

import com.train.merged_pojo.UserWithCity;
import com.train.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserWithCity getById(Integer id) {
        return userRepository.selectById(id);
    }

}
