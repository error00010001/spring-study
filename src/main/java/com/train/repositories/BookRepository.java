package com.train.repositories;

import com.tej.JooQDemo.jooq.sample.model.tables.pojos.Book;
import com.tej.JooQDemo.jooq.sample.model.tables.records.BookRecord;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.tej.JooQDemo.jooq.sample.model.tables.Book.BOOK;

/**
 * Компонент для работы с базой данных.
 */
@Repository
public class BookRepository {

    @Autowired
    private DSLContext context;

    /**
     * Сделать выборку по всем записям
     *
     * @return
     */
    public List<Book> getAll() {
        return context.select()
                .from(BOOK)
                .fetch()
                .map(
                        r -> new Book(
                                r.get(BOOK.ID),
                                r.get(BOOK.TITLE),
                                r.get(BOOK.AUTHOR),
                                r.get(BOOK.PAGES),
                                r.get(BOOK.COST)
                        )
                );
    }

    /**
     * Получить необходимую запись
     *
     * @param id
     * @return
     */
    public Book getById(Integer id) {
        var result = context
                .select()
                .from(BOOK)
                .where(BOOK.ID.eq(id))
                .fetchOne();
        if(result == null) return null;
        return result.map(
                r -> new Book(
                        r.get(BOOK.ID),
                        r.get(BOOK.TITLE),
                        r.get(BOOK.AUTHOR),
                        r.get(BOOK.PAGES),
                        r.get(BOOK.COST)
                )
        );
    }

    /**
     * Добавить запись
     *
     * @param book
     */
    public Integer insert(Book book) {
        BookRecord result = context
                .insertInto(BOOK, BOOK.TITLE, BOOK.AUTHOR, BOOK.PAGES, BOOK.COST)
                .values(
                        book.getTitle(),
                        book.getAuthor(),
                        book.getPages(),
                        book.getCost()
                )
                .returning(BOOK.ID)
                .fetchOne();
        return result.getId();
    }

    /**
     * Обновляет конкретную запись
     *
     * @param id
     * @param book
     */
    public void update(Integer id, Book book) {
        context.update(BOOK)
                .set(BOOK.TITLE, book.getTitle())
                .set(BOOK.AUTHOR, book.getAuthor())
                .set(BOOK.PAGES, book.getPages())
                .set(BOOK.COST, book.getCost())
                .where(BOOK.ID.eq(id))
                .execute();
    }

    /**
     * Удаляет конкретную запись
     *
     * @param id
     */
    public void delete(Integer id) {
        context.delete(BOOK)
                .where(BOOK.ID.eq(id))
                .execute();
    }

    /**
     * Проверяем наличие данной книги
     *
     * @param id
     * @return
     */
    public boolean isExist(int id) {
        return context.fetchExists(
                context.selectFrom(BOOK)
                        .where(BOOK.ID.eq(id))
        );
    }
}
