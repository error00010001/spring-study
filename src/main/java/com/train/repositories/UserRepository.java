package com.train.repositories;

import com.train.merged_pojo.UserWithCity;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.tej.JooQDemo.jooq.sample.model.tables.City.CITY;
import static com.tej.JooQDemo.jooq.sample.model.tables.User.USER;

@Repository
public class UserRepository {

    @Autowired
    DSLContext context;

    /**
     * Пример реализации leftJoin, в данном случае пользователей будет больше чем городов, а значит
     * проще прибавить к User таблицу City
     *
     * @param id
     * @return
     */
    public UserWithCity selectById(Integer id) {
            var record = context
                    .select()
                    .from(USER)
                    .leftJoin(CITY)
                    .on(CITY.ID.eq(USER.ID_CITY))
                    .where(USER.ID.eq(id))
                    .fetchOne();
            if(record == null) return null;
            return record.map(
                    r -> new UserWithCity(
                            r.get(USER.ID),
                            r.get(USER.NAME),
                            r.get(USER.ID_CITY),
                            r.get(CITY.NAME)
                    )
            );
    }
}
