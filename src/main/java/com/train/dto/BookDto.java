package com.train.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Data Transfer Object (DTO) — один из шаблонов проектирования,
 * используется для передачи данных между подсистемами приложения.
 *
 * Библиотеки позволяют нам реализовать валидацию входящих данных.
 *
 * Но для работы еще нам требуется
 * конвертировать сущности для запросов в базу данных
 */
@Data /* компонент из lombok генерирует сеттеры и геттеры для класса */
/*@Builder*/
@AllArgsConstructor
public class BookDto {
    @NotBlank(message = "Необходимо указать название")
    private String title;

    @NotBlank(message = "Необходимо указать автора")
    private String author;

    @NotNull(message = "Необходимо указать количество страниц книги")
    private Integer pages;

    @NotNull(message = "Необходимо указать стоимость в рублях")
    private Integer cost;
}
