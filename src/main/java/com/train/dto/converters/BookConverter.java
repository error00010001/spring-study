package com.train.dto.converters;

import com.tej.JooQDemo.jooq.sample.model.tables.pojos.Book;
import com.train.dto.BookDto;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Дополнительный компонент, чтобы можно было преобразовывать из
 * разных типов в необходимый для слоя API при формировании ответа и
 * наоборот.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BookConverter {

    public BookDto toDto(Book book) {
        return new BookDto(
                book.getTitle(),
                book.getAuthor(),
                book.getPages(),
                book.getCost()
        );
    }

    public Book toModel(BookDto bookDto) {
        var book = new Book();
        book.setTitle(bookDto.getTitle());
        book.setAuthor(bookDto.getAuthor());
        book.setPages(bookDto.getPages());
        book.setCost(bookDto.getCost());
        return book;
    }
}
